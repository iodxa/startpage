var searchForm = document.getElementById('search'),
	searchInput = searchForm.querySelector('input');

if (!searchForm || !searchInput) {
	throw new Error('The search form isnt properly configured!');
}

searchForm.addEventListener('submit', function(event) {
	var tag = '';

	// check if the search starts with a !
	if (searchInput.value[0] === '!') {
		// get the first words index (up to the first space)
		var temp = searchInput.value.indexOf(' ');

		// get a tag for the desired search engine
		tag = searchInput.value.substr(1, temp - 1);

		// remove the tag from the searchInput
		searchInput.value = searchInput.value.substr(temp + 1, searchInput.value.length);
	}

	switch (tag) {
		case 'd':
		case 'ddg':
		case 'duckduckgo':
			searchForm.setAttribute('action', 'https://duckduckgo.com/');
			searchInput.setAttribute('name', 'q');
			break;

		case 'b':
		case 'bing':
			searchForm.setAttribute('action', 'https://www.bing.com/search')
			searchInput.setAttribute('name', 'q');
			break;

		case 'yt':
		case 'youtube':
			searchForm.setAttribute('action', 'https://www.youtube.com/results');
			searchInput.setAttribute('name', 'q');
			break;

		case 'wa':
		case 'wolfram':
		case 'wolframalpha':
			searchForm.setAttribute('action', 'https://www.wolframalpha.com/input/');
			searchInput.setAttribute('name', 'i');
			break;

		case 'g':
		case 'google':
		default:
			searchForm.setAttribute('action', 'https://www.google.com/search')
			searchInput.setAttribute('name', 'q');
			break;
	}
});
